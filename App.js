import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';

import { actionCreators } from './src/redux/todoListRedux'

import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import 'react-native-gesture-handler';

import HomeScreen from './src/pages/Home'
import LoginScreen from './src/pages/LoginScreen'
import AboutScreen from './src/pages/AboutScreen'
import RegisterScreen from './src/pages/RegisterScreen'
import SearchScreen from './src/pages/SearchScreen'
import ListScreen from './src/pages/ListScreen'
import DetailScreen from './src/pages/DetailScreen'
import Splash from './src/pages/Splash'

const mapStateToProps = (state) => ({
  todos: state.todos,
})

import Ionicons from '@expo/vector-icons/Ionicons';

const Tab = createBottomTabNavigator()
const Drawwer = createDrawerNavigator()
const Stack = createStackNavigator()

class App extends Component {
  render() {

    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Login Screen" component={LoginScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
          <Stack.Screen name="Home Screen" component={HomeScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Register" component={RegisterScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Main App" component={MainApp} options={{ headerShown: false }} />
          <Stack.Screen name="Beranda" component={MyDrawwer} options={{ headerShown: false }} />
          <Stack.Screen name="Detail movie" component={DetailScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const MainApp = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Home') {
          iconName = focused
            ? 'home'
            : 'home-outline';
        } else if (route.name === 'Search') {
          iconName = focused ? 'search-circle' : 'search-circle-outline';
        } else if (route.name === 'List') {
          iconName = focused ? 'list-circle' : 'list-circle-outline';
        }

        // You can return any component that you like here!
        return <Ionicons name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#6C5CE7',
      inactiveTintColor: 'gray',
    }}
  >
    <Tab.Screen name="Home" component={HomeScreen} />
    <Tab.Screen name="Search" component={SearchScreen} />
    <Tab.Screen name="List" component={ListScreen} />
  </Tab.Navigator>
)

const MyDrawwer = () => (
  <Drawwer.Navigator>
    <Drawwer.Screen name="App" component={MainApp} />
    <Drawwer.Screen name="About Screen" component={AboutScreen} />
  </Drawwer.Navigator>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});

export default connect(mapStateToProps)(App)

