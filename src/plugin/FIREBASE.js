import firebase from 'firebase'

firebase.initializeApp({
    apiKey: "AIzaSyA4JvhS9eHeQmHZKTqW3oIPfLQL64DgjXg",
    authDomain: "batmovi.firebaseapp.com",
    projectId: "batmovi",
    storageBucket: "batmovi.appspot.com",
    messagingSenderId: "812886666662",
    appId: "1:812886666662:web:77a15d6169303384bd7367"
})

const FIREBASE = firebase;

export default FIREBASE;