import React, { useState, useEffect } from 'react'
import { Button, StyleSheet, Text, View, SafeAreaView, FlatList, Image, TouchableOpacity, Alert, TextInput } from 'react-native'
import FIREBASE from '../plugin/FIREBASE'
import axios from 'axios'
import { Ionicons } from '@expo/vector-icons'
import { LogBox } from 'react-native';
import { useObjectVal } from 'react-firebase-hooks/database';

export default function ListScreen({ route, navigation }) {
    const [items, setItems] = useState([])
    const [Idimdb, setIdimdb] = useState("")
    const [user, setUser] = useState("")
    const [jmlFilm, setjmlFilm] = useState(0)

    const data = {

    }

    const logout = () => {
        Alert.alert(
            "Logout",
            "Yakin anda ingin keluar?",
            [
                { text: "Cancel", onPress: () => console.log("batal"), style: 'cancel' },
                {
                    text: "OK", onPress: () => {
                        FIREBASE.auth().signOut()
                            .then(() => {
                                console.log("user berhasil logout");
                                navigation.navigate("Login Screen");
                            })
                    }
                }
            ],
            { cancelable: false }
        );
    }





    const detailData = (imdbid) => {
        setIdimdb(imdbid)
        navigation.navigate("Detail movie", { idImdb: imdbid, status: "tonton" })
    }

    const ambilData = () => {
        if (!loading && snapshots) {
            if (snapshots) {
                const arr = Object.keys(snapshots).map((k) => snapshots[k])
                setItems(arr)
                setjmlFilm(arr.length)
            } else {
                setItems([])
            }



        }
    }
    const namaTabel = user.uid + '-list'
    const [snapshots, loading, error] = useObjectVal(FIREBASE.database().ref(namaTabel));

    const hapusData = (kode) => {
       
        if (jmlFilm > 1) {
            Alert.alert(
                'Info',
                'Anda yakin akan menghapus Data film (no refund) ?',
                [
                    {
                        text: 'Cancel',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'OK',
                        onPress: () => {

                            var ref = FIREBASE.database().ref(namaTabel);
                            var query = ref.orderByChild("imdbId").equalTo(kode);
                            query.once("value", (snapshot) => {
                                snapshot.forEach((child) => {
                                    child.ref.remove();
                                })
                                setjmlFilm(jmlFilm - 1)
                                ambilData();
                                Alert.alert('Hapus', 'Sukses Hapus Data');
                            })


                        },
                    },
                ],
                { cancelable: false },
            );
        } else {
            Alert.alert('Gagal Hapus', 'Anda tidak boleh menghapus semua film, minimal harus ada satu');
        }
    }

    useEffect(() => {
        const userInfo = FIREBASE.auth().currentUser
        setUser(userInfo)


    }, []

    )

    return (


        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between", padding: 16 }}>
                <View>
                    <Image
                        style={{ height: 50, width: 100, resizeMode: 'contain' }}
                        source={require('../asset/batmovie-logo.png')}
                    />
                </View>
                <View>
                    <TouchableOpacity>
                        <Ionicons name="md-power" size={24} color="#6C5CE7" onPress={logout} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: "space-between", padding: 16 }}>
                <View>
                    <Text style={{ color: "#6C5CE7", fontWeight: 'bold' }}>Daftar Tontonan Anda</Text>
                </View>
                <View>
                    <TouchableOpacity>
                        <Ionicons name="reload-circle-outline" size={32} color="#6C5CE7" onPress={() => ambilData()} />
                    </TouchableOpacity>
                </View>
            </View>
            <SafeAreaView style={{ alignItems: 'center', marginBottom: 50, paddingBottom: 50 }}>


                <FlatList

                    data={items}
                    initialNumToRender={50}
                    keyExtractor={(item) => `${item.imdbId}`}
                    numColumns={2} //membuat 2 kolom flatlist
                    renderItem={({ item }) => {

                        return (
                            <>
                                <View style={styles.content}>

                                    <TouchableOpacity onPress={() => detailData(item.imdbId)}>
                                        <Image

                                            style={{ height: 175, width: 120, margin: 10, }}
                                            source={{ uri: item.poster }}
                                        />
                                    </TouchableOpacity>
                                    <Text style={{ textAlign: 'center' }}>{item.title}</Text>

                                    <Text>{item.year} - {item.imdbRating}</Text>
                                    <TouchableOpacity style={{ margin: 5 }}>
                                        <Ionicons name="md-trash-outline" size={24} color="red" onPress={() => hapusData(item.imdbId)} />
                                    </TouchableOpacity>

                                </View>
                            </>
                        )
                    }
                    }
                />
                <View>
                    <TouchableOpacity>
                        <Ionicons name="reload-circle-outline" size={32} color="#6C5CE7" onPress={() => ambilData()} />
                    </TouchableOpacity>
                    <Text>refresh</Text>
                </View>
            </SafeAreaView>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    content: {
        width: 150,
        height: 350,
        margin: 5,
        alignItems: 'center',
        alignContent: 'space-around',
        borderRadius: 5,
        borderColor: '#6C5CE7',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        padding: 5
    }
})
