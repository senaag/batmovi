import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
const Splash = ({ route, navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.navigate("Beranda")
        }, 3000)
    }, [])

    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, backgroundColor: '#6C5CE7' }}>
            <Image
                style={styles.tinyLogo}
                source={require('../asset/batmovie-logo2.png')}
            />
            <Text>create by sena @2021</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    tinyLogo: {
        width: 200,
        height: 300,
        resizeMode: 'contain'
    },
})
