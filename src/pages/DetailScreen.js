import React, { useState, useEffect } from 'react'
import { Image, ScrollView, StyleSheet, Text, View, Button, Alert } from 'react-native'
import axios from 'axios'
import FIREBASE from '../plugin/FIREBASE'
import { LogBox } from 'react-native';

export default function DetailScreen({ route, navigation }) {
    const { idImdb, status } = route.params;
    const [detils, setDetils] = useState([])
    const [user, setUser] = useState("")

    const GetDetail = () => {
        axios.get(`http://www.omdbapi.com/?i=` + idImdb + `&apikey=88acf8e&plot=full`)
            .then(res => {
                const data1 = (res.data)
                console.log('res: ', data1)
                setDetils(data1)
            }).catch(err => {
                navigation.goBack()
            });


    }

    const pilihFilm = () => {
        if (status == "sewa") {
            Alert.alert(
                "Anda mau membeli film ini",
                "Anda akan dilanjutkan ke halaman pembayaran, dan jika pembayaran berhasil, maka film akan muncul di list anda",
                [
                    { text: "Cancel", onPress: () => console.log("batal"), style: 'cancel' },
                    {
                        text: "OK", onPress: () => {

                            let date = new Date();
                            let isodate = date.toISOString();
                            const film = {
                                title: detils.Title,
                                imdbId: detils.imdbID,
                                year: detils.Year,
                                imdbRating: detils.imdbRating,
                                genre: detils.Genre,
                                rated: detils.Rated,
                                poster: detils.Poster,
                                awalSewa: isodate
                            }

                            //LogBox.ignoreLogs(['Setting a timer']);
                            const namaTabel = user.uid + '-list'
                            FIREBASE.database().ref('/' + namaTabel)
                                .push(film)
                                .then(() => {

                                    Alert.alert('Sukses', 'data film tersimpan');
                                    navigation.navigate("List");
                                })
                                .catch((error) => {
                                    console.log("Error : ", error);
                                })

                        }
                    }
                ],
                { cancelable: false }
            );
        } else {
            Alert.alert('tonton', 'fitur tonton sedang dalam pengembangan');
        }

    }

    useEffect(() => {
        const userInfo = FIREBASE.auth().currentUser
        setUser(userInfo)
        GetDetail()
    }, []
    )

    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={{ textAlign: 'center', fontSize: 20, fontWeight: 'bold', color: '#6C5CE7' }}>{detils.Title}</Text>
                <Text>{detils.Year}</Text>
                <Image
                    style={{ height: 400, width: 300, margin: 10, resizeMode: 'contain' }}
                    source={{ uri: detils.Poster }}
                />
                <Text style={{ textAlign: 'center' }}>{detils.Genre}</Text>
                <Text>Imdb Rating : {detils.imdbRating}</Text>
                <Text>Runtime : {detils.Runtime}</Text>
                <View style={{ marginLeft: 5 }}>
                    <Text style={{ textAlign: 'left' }}>Director : {detils.Director}</Text>
                    <Text style={{ textAlign: 'left' }}>Actors : {detils.Actors}</Text>
                    <Text style={{ textAlign: 'left' }}>Plot : {detils.Plot}</Text>
                </View>
                <Button
                    title={status}
                    onPress={pilihFilm}
                />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        padding: 5

    },

})
