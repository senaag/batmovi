import React, { useState } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, ScrollView, Alert } from 'react-native'
import FIREBASE from '../plugin/FIREBASE'

export default function RegisterScreen({ navigation }) {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [password2, setPassword2] = useState("")

    
    const submit = () => {
        const data = {
            email, password
        }
        console.log(data)

        if (email == "" || password == "" || password2 == "") {
            Alert.alert(
                "Data belum lengkap",
                "Isi dahulu data anda dengan lengkap",
                [
                    { text: "OK", onPress: () => console.log("data belum lengkap") }
                ],
                { cancelable: false }
            );
        } else if (password != password2) {
            Alert.alert(
                "password berbeda",
                "Anda harus mengulang password yang sama",
                [
                    { text: "OK", onPress: () => console.log("password berbeda") }
                ],
                { cancelable: false }
            );
        } else {
            FIREBASE.auth().createUserWithEmailAndPassword(email, password)
                .then(() => {
                    console.log('User account created & signed in!');
                    navigation.navigate("Beranda");
                })
                .catch(error => {
                    if (error.code === 'auth/email-already-in-use') {
                        Alert.alert('Gagal Daftar', 'email sudah digunakan');
                        console.log('That email address is already in use!');
                    }

                    if (error.code === 'auth/invalid-email') {
                        Alert.alert('Gagal Daftar', 'format email salah');
                        console.log('That email address is invalid!');
                    }

                    if (error.code === 'auth/invalid-password') {
                        Alert.alert('Gagal Daftar', 'email minimal harus 6 karakter');
                        console.log('That email address is invalid!');
                    }
                    console.error(error);
                });
        }
    }



    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={{width:250, height:250, resizeMode: 'contain'}}
                        source={require('../asset/batmovie-logo2.png')}
                    />
                </View>
                <Text style={styles.formTitle}>Register</Text>
                <View style={styles.form}>

                  
                    <TextInput
                        placeholder="Masukan email"
                        style={styles.inputText}
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                    />
                
                    <TextInput
                        placeholder="Masukan password"
                        style={styles.inputText}
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />
                
                    <TextInput
                        placeholder="ulangi lagi password"
                        style={styles.inputText}
                        secureTextEntry={true}
                        value={password2}
                        onChangeText={(value) => setPassword2(value)}
                    />

                </View>
                <View style={{ alignItems: 'center', marginTop: 20 }}>
                    <TouchableOpacity
                        onPress={submit}
                    >
                        <View style={styles.buttonDark}>
                            <Text style={{ fontSize: 18, color: 'white' }}>DAFTAR</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 18, marginBottom: 5 }}>sudah punya akun?, </Text>
                        <Text style={{ fontSize: 18, marginBottom: 5, color: 'blue' }} onPress={() => navigation.navigate("Login Screen")}>login</Text>
                    </View>
                </View>

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center'
    },
    header: {
        marginTop: 30,
        justifyContent: 'center'
    },
    form: {
        //flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
    },
    formTitle: {
        color: '#6C5CE7',
        fontSize: 18,
        fontWeight: 'bold',
        paddingBottom: 5,
        marginTop: 10,
        marginBottom: 10
    },
    formInput: {
        backgroundColor: 'white',
        justifyContent: 'flex-start'
    },
    inputText: {
        width: 300,
        borderColor: '#6C5CE7',
        borderWidth: 1,
        paddingVertical: 10,
        marginBottom: 10,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        borderRadius: 10
    },
    buttonDark: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: '#6C5CE7',
        borderRadius: 20,
        marginBottom: 10
    },
    buttonLight: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#6C5CE7',
        borderRadius: 20,
        marginBottom: 10
    }
})
