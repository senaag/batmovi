import React, { useState } from 'react'
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput, ScrollView, Alert } from 'react-native'
import FIREBASE from '../plugin/FIREBASE'

export default function LoginScreen({ route, navigation }) {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")


    const submit = () => {

        if (email == "" || password == "") {
            Alert.alert(
                "Data belum lengkap",
                "Isi dahulu data anda dengan lengkap",
                [
                    { text: "OK", onPress: () => console.log("data belum lengkap") }
                ],
                { cancelable: false }
            );
        } else {
            FIREBASE.auth().signInWithEmailAndPassword(email, password)
                .then(() => {
                    console.log('User account signed in!');
                    navigation.navigate("Splash");
                })
                .catch(error => {
                    Alert.alert('Gagal Login', 'email atau password anda salah');
                    console.error(error);
                });
        }
    }
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={{width:250, height:250, resizeMode: 'contain'}}
                        source={require('../asset/batmovie-logo2.png')}
                    />
                </View>
                <Text style={styles.formTitle}>Login</Text>
                <View style={styles.form}>


                    <TextInput
                        placeholder="Masukan email"
                        style={styles.inputText}
                        value={email}
                        onChangeText={(value) => setEmail(value)}
                    />

                    <TextInput
                        placeholder="Masukan Password"
                        style={styles.inputText}
                        secureTextEntry={true}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                    />

                </View>
                <View style={{ alignItems: 'center', marginTop: 20 }}>

                    <TouchableOpacity
                        onPress={submit}
                    >
                        <View style={styles.buttonLight}>
                            <Text style={{ fontSize: 18, color: '#6C5CE7' }}>MASUK</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 18, marginBottom: 5 }}>belum punya akun?, </Text>
                        <Text style={{ fontSize: 18, marginBottom: 5, color: 'blue' }} onPress={() => navigation.navigate("Register")}>daftar</Text>
                    </View>

                </View>

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: "space-around",
        alignItems: 'center'
    },
    header: {
        marginTop: 30,
        justifyContent: 'center'
    },
    form: {
        //flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-start',
    },
    formTitle: {
        color: '#6C5CE7',
        fontSize: 18,
        fontWeight: 'bold',
        paddingBottom: 5,
        marginTop: 10,
        marginBottom: 10
    },
    formInput: {
        backgroundColor: 'white',
        justifyContent: 'flex-start'
    },
    inputText: {
        width: 300,
        borderColor: '#6C5CE7',
        borderWidth: 1,
        paddingVertical: 10,
        marginBottom: 10,
        marginHorizontal: 10,
        paddingHorizontal: 10,
        borderRadius: 10
    },
    buttonDark: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: '#6C5CE7',
        borderRadius: 20,
        marginBottom: 10
    },
    buttonLight: {
        height: 40,
        width: 100,
        alignItems: 'center',
        paddingHorizontal: 5,
        paddingVertical: 5,
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#6C5CE7',
        borderRadius: 20,
        marginBottom: 10
    }
})
