import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, SafeAreaView, FlatList, Image, TouchableOpacity, Alert } from 'react-native'
import FIREBASE from '../plugin/FIREBASE'
import axios from 'axios'
import { Ionicons } from '@expo/vector-icons'
import { LogBox } from 'react-native';

export default function Home({ route, navigation }) {
    const [items, setItems] = useState([])
    const [Idimdb, setIdimdb] = useState("")
    const [user, setUser] = useState("")
    const data = {

    }

    const namaTabel = user.uid + '-list'

    const logout = () => {
        Alert.alert(
            "Logout",
            "Yakin anda ingin keluar?",
            [
                { text: "Cancel", onPress: () => console.log("batal"), style: 'cancel' },
                {
                    text: "OK", onPress: () => {
                        FIREBASE.auth().signOut()
                            .then(() => {
                                console.log("user berhasil logout");
                                navigation.navigate("Login Screen");
                            })
                    }
                }
            ],
            { cancelable: false }
        );
    }

    const GetData = () => {
        axios.get(`http://www.omdbapi.com/?s=batman&page=2&apikey=88acf8e`)
            .then(res => {
                const data1 = (res.data.Search)
                console.log('res: ', data1)
                setItems(data1)
            })
    }

    const detailData = (imdbid) => {
        setIdimdb(imdbid)
        //LogBox.ignoreLogs();
        var ref = FIREBASE.database().ref(namaTabel);
        var query = ref.orderByChild("imdbId").equalTo(imdbid);
        var stat = "sewa"
        query.once("value", (snapshot) => {
            console.log(snapshot.val());
            snapshot.forEach((data) => {
                console.log(data.key);
                stat = "tonton";
            });
            navigation.navigate("Detail movie", { idImdb: imdbid, status: stat })
        })


    }

    useEffect(() => {
        const userInfo = FIREBASE.auth().currentUser
        setUser(userInfo)
        GetData()
    }, []

    )

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: "space-between", padding: 16 }}>
                <View>
                    <Image
                        style={{ height: 50, width: 100, resizeMode: 'contain' }}
                        source={require('../asset/batmovie-logo.png')}
                    />
                </View>
                <View>
                    <TouchableOpacity>
                        <Ionicons name="md-power" size={24} color="#6C5CE7" onPress={logout} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: "space-between", padding: 16 }}>
                <View>
                    <Text>Selamat Datang,</Text>

                </View>
                <View>
                    <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#6C5CE7' }}>{user.email}</Text>
                </View>
            </View>
            <SafeAreaView style={{ alignItems: 'center', marginBottom: 50, paddingBottom: 50 }}>


                <FlatList
                    data={items}
                    initialNumToRender={50}
                    keyExtractor={(item) => `${item.imdbID}`}
                    numColumns={2} //membuat 2 kolom flatlist
                    renderItem={({ item }) => {
                        return (
                            <>
                                <View style={styles.content}>
                                    <TouchableOpacity onPress={() => detailData(item.imdbID)}>
                                        <Image

                                            style={{ height: 175, width: 120, margin: 10, }}
                                            source={{ uri: item.Poster }}
                                        />
                                    </TouchableOpacity>
                                    <Text style={{ textAlign: 'center' }}>{item.Title}</Text>

                                    <Text>{item.Year}</Text>

                                </View>
                            </>
                        )
                    }
                    }
                />
                <View>
                    <Text>created by sena</Text>
                </View>
            </SafeAreaView>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    content: {
        width: 150,
        height: 300,
        margin: 5,
        alignItems: 'center',
        alignContent: 'space-around',
        borderRadius: 5,
        borderColor: '#6C5CE7',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
        padding: 5
    }
})
